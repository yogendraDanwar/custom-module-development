<?php

namespace Drupal\custom_form\EventSubscriber;

/**
 * This event will automatically called because of the tags event subscriber
 */
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\custom_form\NodeInsertEvent;
use Drupal\Core\Url;

/**
 *
 */
class AllowOriginEvent implements EventSubscriberInterface{


  function __construct()
  {
    error_log('IN The AllowOriginEvent');
  }

  public function onResponse(FilterResponseEvent $event){

    preg_match('/node/', \Drupal::service('path.current')->getPath(), $matches, PREG_OFFSET_CAPTURE);

    $current_url = Url::fromRoute('<current>')->getInternalPath();

    kint($current_url);

    $frontpage=\Drupal::service('path.matcher')->isFrontPage();

    if($matches[0][0]=="node"&& !$frontpage) {
      kint('iojref');
      error_log('Calling onResponse');
      $response = $event->getResponse();
      $response->headers->set('Access-Control-Allow-Origin', '*');
    }
	}

	public function onSubmit(NodeInsertEvent $event){
		error_log('Calling onSubmit');
		$response=$event->getnodeInfo();
		// Logs a notice
	\Drupal::logger('custom_form')->notice($response->getTitle());
	}

	/**
	 * [getSubscribedEvents description]
	 * @return [type] [description]
	 */
	static function getSubscribedEvents(){
		// error_log('from the getSubscribedEvents');
		$events[KernelEvents::RESPONSE][]=array('onResponse');
		$events[NodeInsertEvent::SUBMIT][]=array('onSubmit');
		return $events;
	}
}