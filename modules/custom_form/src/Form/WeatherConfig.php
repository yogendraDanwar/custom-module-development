<?php


namespace Drupal\custom_form\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\config\Form\ConfigSingleExportForm;
use Drupal\Core\Database\Database;




/**
* 
*/
class WeatherConfig extends ConfigFormBase{
	
	// public function __construct(){
	// 	# code...
	// }

	/**
	 * [getFormId description]
	 * @return [type] [description]
	 */
	public function getFormId(){
        return 'custom.weather_config';
    }

    /**
     * [getEditableConfigNames description]
     * @return [type] [description]
     */
    public function getEditableConfigNames(){
    
    	return ['custom_form_weather.settings.yml'];
    
    }
    /**
     * [buildForm description]
     * @param  array              $form       [description]
     * @param  FormStateInterface $form_state [description]
     * @return [type]                         [description]
     */
    public function buildForm(array $form, FormStateInterface $form_state){
    	
    	$form = parent::buildForm($form, $form_state);
    	$config=$this->config('custom_form_weather.settings');

    	$form['appid']=[
    		'#type'=>'textfield',
    		'#title'=>$this->t('AppId'),
    		'#required'=>TRUE,
    		'#default_value'=>$config->get('weatherconfig.appid'),
    	];

    	return $form;
    }

    /**
     * [validateForm description]
     * @param  array              &$form      [description]
     * @param  FormStateInterface $form_state [description]
     * @return [type]                         [description]
     */
    public function validateForm(array &$form, FormStateInterface $form_state){
    	return parent::validateForm($form,$form_state);
    }

    /**
     * [submitForm description]
     * @param  array              &$form      [description]
     * @param  FormStateInterface $form_state [description]
     * @return [type]                         [description]
     */
    public function submitForm(array &$form, FormStateInterface $form_state){
    	$config = $this->config('custom_form_weather.settings');
    	$config->set('weatherconfig.appid', $form_state->getValue('appid'));
  		$config->save();
  		
//  		print $config->get('weatherconfig.appid');
  		
  		return parent::submitForm($form, $form_state);
    }

}



