<?php


/*
Contains The code for Directory Form in PHP Drupal
*/

namespace Drupal\custom_form\Form;

/**
 * FormBase, FormStateInterface, UrlHelper
 */

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\CssCommand;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\custom_form\CustomFormService;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;

/**
 * class DirectoryForm extending FormBase
 */
class DirectoryForm extends FormBase implements ContainerInjectionInterface
{

    protected $customFetchService;
    protected $conn;
    protected $selected;
    protected $options;
    protected $response;
    protected $cities;

    

    public static function create(ContainerInterface $container)
    {

        // Load the service required to construct this class.
        return new static($container->get('custom_form.fetchservice'));

    }


    public function __construct(CustomFormService $customFetchService)
    {
        // Write some regarding the constructor of DirectoryForm
        // kint($customFetchService->fetchData());
        $this->conn = Database::getConnection();
        $this->customFetchService = $customFetchService;
        $this->response = new AjaxResponse();
        $this->selected='';
        // $this->cities[1] = ['Kolkata', 'Mumbai', 'Jaipur'];
        // $this->cities[2] = ['London', 'Scottland'];

         $this->cities = array(
            1 => array(
                'Kolkata',
                'Mumbai',
                'Jaipur'
                ),
            2 => array(
                'London',
                'Scottland'
                )
            );
    }

    function getFormId()
    {
        return 'custom.directory_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {

//

//        print_r($selected);

        $form['firstname'] = [
            '#type' => 'textfield',
            '#title' => $this->t('FirstName'),
            '#required' => TRUE,
            '#states' => [
                'invisible' => [
                    ':input[name="testcheckbox"]' => ['checked' => TRUE],
                ],
            ],
        ];

        $form['EmailId'] = [
            '#type' => 'email',
            '#title' => $this->t('Email ID'),
            '#required' => TRUE,
            '#ajax' => [
                'callback' => array($this, 'validateEmailAjax'),
                'event' => 'change',
                'progress' => array(
                    'type' => 'throbber',
                    'message' => t('Verifying ...'),
                ),
            ],
            '#suffix'=>'<span class="email-valid-message"></span>'
        ];

        $form['lastname'] = [
            '#type' => 'textfield',
            '#title' => $this->t('LastName'),
            '#required' => FALSE,
            '#states' => [
                'invisible' => [
                    ':input[name="testcheckbox"]' => ['checked' => TRUE],
                ],
            ],
        ];

        $form['testcheckbox'] = [
            '#type' => 'checkbox',
            '#title' => $this->t('Anonymous User'),
        ];

        $form['qualification']=[
            '#type'=>'select',
            '#title'=>$this->t('Qualification'),
            "#empty_option"=>t('- Select -'),
//            '#default_value' => 2, // for by default selecting a value
            '#options'=>[
                1 =>$this->t('U.G'),
                2 =>$this->t('P.G'),
                3 =>$this->t('Other'),
            ],
        ];

        $form['ugqualification']=[
            '#type'=>'textfield',
            '#title'=>$this->t('Others'),
            '#states'=>[
              'visible'=>[
                  ':input[name="qualification"]'=> ['value' => 3],
              ]
            ],
        ];

        $form['country']=[
            '#type'=>'select',
            '#title'=>$this->t('Country'),
            '#empty_option'=>$this->t('-Select-'),
            '#options'=>[
                1 =>$this->t('India'),
                2 =>$this->t('UK'),
            ],
            '#ajax' => [
                'callback' => array($this, '_ajax_country_callback'),
                'wrapper' => 'dropdown-second-replace',
                'event' => 'change',
                'progress' => array(
                    'type' => 'throbber',
                    'message' => t('CheckingList...'),
                ),
            ],
        ];

        $form['city']=[
            '#type'=>'select',
            '#title'=>$this->t('City'),
            '#empty_option'=>$this->t('-Select-'),
            '#prefix' => '<div id="dropdown-second-replace">',
            '#suffix' => '</div>',
//            '#options'=>$this->_ajax_country_list($this->selected),
            '#options'=>$this->_ajax_country_list($form_state->getValue('country')),
        ];

        $form['actions']['submit'] = [
            '#type' => 'submit',
            '#value' => $this->t('Submit'),
            '#button_type' => 'primary',
        ];

        return $form;
    }

    public function _ajax_country_callback(array $form,FormStateInterface $form_state) {

       // if(!empty($form_state->getValue('country')) || $form_state->getValue('country')!=0){
        
//        $this->selected=$form_state->getValue('country');
        // print_r($form_state->getValue('country'));
        // print_r($this->options);
      return $form['city'];

//        return $form['city']['#options'] = $this->cities[$this->selected];
    
    }

    public function _ajax_country_list($options){

        if($options==1)
            return array_combine(
                array(
                    1,2,3,4,
                )
                ,array(
                    $this->t('Jaipur'),
                    $this->t('BLABLA'),
                    $this->t('LOALAOLA'),
                    $this->t('ROFLROFLROFL'),
                )
            );
        else if($options==2)
            return array_combine(array(
                1,
                2,
                3,
                4,
            ),array(
                $this->t('London'),
                $this->t('Scottland'),
                $this->t('Bath'),
                $this->t('bradford'),
            ));

        return [];
    }


    /**
     * Validates that the email field is correct.
     */
    protected function validateEmail(array &$form, FormStateInterface $form_state)
    {
        if (substr($form_state->getValue('EmailId'), -4) !== '.com') {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * Ajax callback to validate the email field.
     */
    public function validateEmailAjax(array &$form,FormStateInterface $form_state)
    {

//        print_r($form_state->getValue('EmailId'));

       $valid = $this->validateEmail($form, $form_state);

        if ($valid) {
            $css = ['border' => '2px solid green'];
            $message = $this->t('Email ok.');
        } else {
            $css = ['border' => '2px solid red'];
            $message = $this->t('Email not valid.');
        }

        //This will be applied to input border
        $this->response->addCommand(new CssCommand('#edit-emailid',$css));
        //This will act as html() in jQuery
        $this->response->addCommand(new HtmlCommand('.email-valid-message',$message));

        return $this->response;
    }

    /**
     * [validateForm description]
     * @param  array &$form [description]
     * @param  FormStateInterface $form_state [description]
     * @return [type]                         [description]
     */
    public function validateForm(array &$form, FormStateInterface $form_state)
    {

    }

    /**
     * {@inheritdoc}
     */
    public function submitForm(array &$form, FormStateInterface $form_state)
    {
      if (!$form_state->getValue('firstname')){
        kint($form_state->getFormObject());

        $this->conn->insert('d8_demo')->
        fields(
          array(
            'firstname' => $form_state->getValue('firstname'),
            'lastname' => $form_state->getValue('lastname'),
          )
        )->execute();

        drupal_set_message($this->t($this->customFetchService->fetchData()), 'status', FALSE);

      }


    }
}


