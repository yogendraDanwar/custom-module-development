<?php

/*
Contains The code for Creating the PHP Custom Forums
*/

namespace Drupal\custom_form\Form;


use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\UrlHelper;


class ContributionForm extends FormBase {
    /**
   * {@inheritdoc}
   */
  public function getFormId() {
      return 'custom.contributionform';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    
    $form['title'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#required' => TRUE,
    );
    
    $form['mail'] = array(
      '#type' => 'email',
      '#title' => $this->t('Email ID:'),
      '#required' => TRUE,
    );
    
    $form['candidate_dob'] = array (
      '#type' => 'date',
      '#title' => $this->t('DOB'),
      '#required' => TRUE,
    );

    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    );

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    
        //$form_state consist the value from the form
        //$form_state['values']['nameoffield']
       if (strlen($form_state->getValue('title')) <= 5) {
        $form_state->setErrorByName('title', $this->t('Title is too short.'));
      }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
    // Display result.
    foreach ($form_state->getValues() as $key => $value) {
      if ($key=='title'&&strlen($value)>=5) 
        drupal_set_message($this->t('Form Submitted Successfully'), 'status', FALSE);
      else if($key=='title'&&strlen($value)<5)
        drupal_set_message($this->t('Warning'),'error',FALSE);
    }
  }

}