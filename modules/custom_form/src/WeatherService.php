<?php

namespace Drupal\custom_form;
use GuzzleHttp\Client;

/**
 * Class WeatherService
 * @package Drupal\custom
 */
class WeatherService{

    /**
     * @var string
     */
    protected $client;
    protected $response;

    function __construct(Client $client){
        $this->client=$client;

        error_log("Calling From the WeatherService construct");
    }

    function fetchWeatherReport($cityname,$appid){
        error_log("Calling From fetchWeatherReport function ");
        $this->response=$this->client->request('GET','http://api.openweathermap.org/data/2.5/weather?q='.$cityname.'&'.'appid='.$appid);
        
        return (string)$this->response->getBody(); 

    }

}