<?php

/**
* @file providing the service.
*
*/
namespace Drupal\custom_form;

use Drupal\Core\Database\Connection;

class CustomFormService{
	
	/**
	 * [$connection description]
	 * @var [type]
	 */
	protected $connection;
	protected $output;

	/**
	 * [__construct description]
	 * @param Connection $connection [description]
	 */
	public function __construct(Connection $connection){
		$this->connection=$connection;
	}

	public function GenerateConnection(){

		$query = $this->connection->select('d8_demo', 'demo');
		$query->fields('demo', ['firstname', 'lastname']);
		$result = $query->execute();	
		return $result;
	}

	public function fetchData(){
		$result=$this->GenerateConnection();
		$this->output='';
		while ($row = $result->fetchAssoc()) {
			$this->output .= $row['firstname'];
			 $this->output .= $row['lastname'];
		}
		return $this->output;
	}
}