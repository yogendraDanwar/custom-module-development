<?php
namespace Drupal\custom_form\Controller;

class Caster {

  public function parametercaster($config){
    $result=\Drupal::config('custom_form_weather.settings')->get($config);
    if(empty($result))
      return [
        '#markup'=>'Configuration Not Found'
      ];
    return[
      '#markup'=>'The Config is ::'. $result
    ];
  }

}