<?php
/**
 * @file
 * Contains \Drupal\custom\Plugin\Block\CityBlock.
 */

namespace Drupal\custom_form\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'city' block.
 *
 * @Block(
 *   id = "city_block",
 *   admin_label = @Translation("City block"),
 *   category = @Translation("Custom City Block")
 * )
 */
class CityBlock extends BlockBase{

    protected $weather_service;
    protected $response;
    /**
	 * [build description]
	 * @return [type] [description]
	 */
    public function build() {
      $config = $this->getConfiguration();
      $this->weather_service=\Drupal::service('custom_form.weatherfetchservice');

      // error_log('Calling from Build section');

      $this->response=$this->weather_service->fetchWeatherReport($config['city_name'],\Drupal::config   ('custom_form_weather.settings')->get('weatherconfig.appid'));
        $decoded_response = json_decode($this->response);
        return [
            '#theme' => 'weatherdata_template',
            '#temp_min' =>$decoded_response->main->temp_min,
            '#temp_max' =>$decoded_response->main->temp_max,
            '#pressure' =>$decoded_response->main->pressure,
            '#humidity' =>$decoded_response->main->humidity,
            '#wind_speed' =>$decoded_response->wind->speed
        ];

    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);

        // error_log("Calling from the blockForm");

        // Retrieve existing configuration for this block.
        $config = $this->getConfiguration();

        // Add a form field to the existing block configuration form.
        $form['city_name'] = array(
            '#type' => 'textfield',
            '#title' => t('City Name'),
            '#default_value' => isset($config['city_name']) ? $config['city_name'] : '',
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {

        // error_log("Calling From The blockSubmit");

        // Save our custom settings when the form is submitted.
        $this->setConfigurationValue('city_name', $form_state->getValue('city_name'));
    }

}

