<?php

namespace Drupal\custom_form\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use AntoineAugusti\Books\Fetcher;
use GuzzleHttp\Client;

/**
 * Provides a 'ISBN' block.
 *
 * @Block(
 *   id = "isbn_block",
 *   admin_label = @Translation("ISBN Block"),
 *   category = @Translation("Custom Isbn Block")
 * )
 */
class ISBNBlock extends BlockBase
{

  public function build()
  {
    $config = $this->getConfiguration();
    $bookrequest = '';
    error_log('in the build section');
    try {
      $client = new Client(['base_uri' => 'https://www.googleapis.com/books/v1/']);
      $fetcher = new Fetcher($client);
      $bookrequest= $fetcher->forISBN($config['isbn']);
//      kint($bookrequest->publishedDate);
      return [
        '#markup' => $bookrequest->title,
      ];
    }catch (\Exception $e){
      error_log($e);
      return [
        '#markup'=>'Error In The Retreiver'
      ];
    }
  }

  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    $form['book_isbn'] = array(
      '#type' => 'textfield',
      '#title' => $this->t('ISBN'),
      '#default_value' => isset($config['isbn']) ? $config['isbn'] : '',
    );
    return $form;
  }

  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->setConfigurationValue('isbn', $form_state->getValue('book_isbn'));
  }
}