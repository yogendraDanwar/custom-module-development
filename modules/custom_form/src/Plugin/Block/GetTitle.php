<?php

namespace Drupal\custom_form\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Database\Database;
/**
 * Provides a 'Title' block.
 *
 * @Block(
 *   id = "gettitle_block",
 *   admin_label = @Translation("Get 3 Title Block"),
 *   category = @Translation("Custom title Block")
 * )
 */
class GetTitle extends BlockBase
{

  public function build()
  {
    $data=Database::getConnection();
    $usermail = \Drupal::currentUser()->getEmail();
    $result=$data->select('node_field_data','n')
      ->fields('n', ['nid','title'])
      ->condition('type','article','=')
      ->orderBy('created', 'DESC')
      ->range(0,3)
      ->execute();
    $options[]='';
    foreach ($result as $key =>$record) {
      $options[$key]['nid']=$record->nid;
      $options[$key]['title']=$record->title;
    }

    kint($options);

    return[
      '#markup'=>
        '1.'.$options[0]['title'].'<br/>'.
        '2.'.$options[1]['title'].'<br/>'.
        '3.'.$options[2]['title'].'<br/>'.
        'Email Id.'.$usermail,
      '#cache'=>[
        'tags'=>['node:'.$options['nid'][0],'node:'.$options['nid'][1],'node:'.$options['nid'][2]],
        'contexts'=>['user']
      ]
    ];
  }

  public function blockForm($form, FormStateInterface $form_state)
  {
    $form = parent::blockForm($form, $form_state);
    // Retrieve existing configuration for this block.
    $config = $this->getConfiguration();
    $form['title_limit']=array(
      '#type' => 'textfield',
      '#title' => $this->t('Limit'),
      '#default_value' => isset($config['limitno']) ? $config['limitno'] : '',
    );
    return $form;
  }

  public function blockSubmit($form, FormStateInterface $form_state)
  {
    $this->setConfigurationValue('limitno',$form_state->getValue('title_limit'));
  }
}