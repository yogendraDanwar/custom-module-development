<?php

namespace Drupal\custom_form\Plugin\rest\resource;

use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Psr\Log\LoggerInterface;

/**
 * Provides a Demo Resource
 *
 * @RestResource(
 *   id = "demo_resource",
 *   label = @Translation("Demo Resource"),
 *   uri_paths = {
 *    "canonical" = "/custom_form/demo_resource",
 *    "https://www.drupal.org/link-relations/create" = "/custom_form/demo_resource"
 *   }
 * )
 */
class DemoResource extends ResourceBase {

  /**
   * Responds to entity GET requests.
   * @return \Drupal\rest\ResourceResponse
   */
  protected $config;

  public function getConfig(){

    $this->config=\Drupal::config('custom_form_weather.settings')->get('weatherconfig.appid');
    return $this->config;
  }
  public function get() {

    $response = [
      'message' => 'Hello, this is a rest service',
      'appid'=>$this->getConfig()
    ];
    return new ResourceResponse($response);
  }


  public function  post(){
    $response=[
      'message' => 'Hello, this is a rest service',
      'appid'=>$this->getConfig()
    ];
    return new ResourceResponse($response);
  }


}