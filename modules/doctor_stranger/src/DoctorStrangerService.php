<?php
/**
* @file providing the service that say hello world and hello 'given name'.
*
*/
namespace  Drupal\doctor_stranger;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Routing\Access\AccessInterface;
use Drupal\Core\Routing\RouteMatch;
use Drupal\Core\Session\AccountProxy;
use Symfony\Component\Routing\Route;


class DoctorStrangerService implements AccessInterface {
    /**
     * @var AccountProxy
     */
 protected $currentUser;
 protected $nodeId;
 protected $loggedUser;

    /**
     * DoctorStrangerService constructor.
     * @param AccountProxy $currentUser
     */
 public function __construct(AccountProxy $currentUser) {
 	
 	$this->currentUser=$currentUser;
 	$this->nodeId=null;
 	$this->loggedUser=null;

 }

    /**
     * @param Route $route
     * @param RouteMatch $routeMatch
     * @return \Drupal\Core\Access\AccessResultAllowed|\Drupal\Core\Access\AccessResultForbidden|\Drupal\Core\Access\AccessResultNeutral
     */
 public function access(Route $route,RouteMatch $routeMatch){

     $this->node_id=$routeMatch->getParameter('node1')->getOwner('uid')->id();
     $this->loggedUser = \Drupal::currentUser()->id();

     if ($this->node_id == $this->loggedUser){

         if ($route->getRequirement('_access_check_node')=='TRUE')
             return AccessResult::allowed();
         elseif ($route->getRequirement('_access_check_node')=='FALSE')
             return AccessResult::forbidden();

         return AccessResult::neutral();

     }



 }

}