<?php

namespace Drupal\configmenu\Controller;

use Drupal\user\Entity\User;

/**
 * Created by PhpStorm.
 * User: yogendra
 * Date: 30/06/17
 * Time: 1:35 PM
 */

class GetListing {

  protected $user;
  function __construct()
  {
    $this->user = \Drupal::currentUser();
  }

  public function getList(){
    $usrpermission=$this->user->hasPermission('access content');

    if ($usrpermission){
      return[

        '#markup'=>'You Have The Permission to view this content',

      ];
    }

    return [];
  }
}