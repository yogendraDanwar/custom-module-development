<?php

namespace Drupal\configmenu\Controller;

use Drupal\user\Entity\User;
/**
 * Created by PhpStorm.
 * User: yogendra
 * Date: 30/06/17
 * Time: 1:41 PM
 */

class UserAccess {

  protected $user;
  function __construct(User $user)
  {
    $this->user=$user;

  }

  public function checkaccess($arg){
    $usrpermission=$this->user->hasPermission('access training content');

    if ($usrpermission){
      return[

        '#markup'=>'You Have The Permission to view this content'.$arg,

      ];
    }

    return [];
  }
}